# ACTA-CFD-1.0.0

Container pour l'équipe ACTA composé de incompact3d, PALM, WRF et OpenFoam en Ubuntu 18.04.
Johan Carlier, Laurence Wallian

## CONTENT

The singularity image is delivered with =>
```
palm_model_system-v22.04
openfoam2112-default
libopenmpi-dev
```
## BUILD
```
singularity build cfd-1.0.0.sif cfd-1.0.0.def
```
## USAGE
First, pull the image then use it like so =>
```
singularity pull cfd-1.0.0.sif oras://registry.forgemia.inra.fr/singularity-mesolr/acta-cfd-1.0.0/acta-cfd-1.0.0:latest
./cfd-1.0.0.sif python3 ...
```

Make Incompact3d =>
```bash
singularity pull acta-cfd-1.0.0.sif oras://registry.forgemia.inra.fr/singularity-mesolr/acta-cfd-1.0.0/acta-cfd-1.0.0:latest

git clone https://github.com/xcompact3d/Incompact3d.git

./acta-cfd-1.0.0.sif bash -c "cd Incompact3d && make clean"
./acta-cfd-1.0.0.sif bash -c "cd Incompact3d && make"

# Row and col at 0
./acta-cfd-1.0.0.sif bash -c "cd Incompact3d && ./xcompact3d examples/Taylor-Green-Vortex/input.i3d"

# Row and col at 4
module load openmpi/psm2/gcc61/2.0.1
mpirun -np 16 ./acta-cfd-1.0.0.sif bash -c "cd Incompact3d && ./xcompact3d examples/Taylor-Green-Vortex/input.i3d"
```
